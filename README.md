# Cabify::Checkout

This gem provides solution Cabify Mobile Challenge.
For more details, please see here: https://github.com/cabify/rubyChallenge

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'cabify-checkout'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install cabify-checkout

## Usage

Simply run console:
```
bin/console
```

Example usage:

```
checkout = Cabify::Checkout.new(pricing_rules)
checkout.scan("VOUCHER")
checkout.scan("VOUCHER")
checkoout.scan("TSHIRT")
checkout.total # => "€30,00"
```
NOTE: Currency symbol on the beginning is default format provided by money gem. 
I don't see the reason to change it :)

Separator can be configured by:
```ruby
I18n.backend.store_translations(:en, number: { format: { separator: "." }
```

For more details, see here: https://github.com/RubyMoney/money

### Available pricing rules (promotions):

```
* :promo_2_for_1          # buy two of the same product, get one free
* :promo_bulk_purchase    # buying x or more of a product, the price of that product is reduced
* nil                     # no promotion
```

Example:

```ruby
pricing_rules = Cabify::PricingRules.new(tshirt: :promo_2_for_1, voucher: nil)

# Products (i.e. :mug, :tshirt, :voucher) without promotion can be omitted on object initialization.
```

Moreover, there is an option to combine a few promotions for an product! For example:

```ruby
Cabify::PricingRules.new(
  voucher: [
    :promo_2_for_1,
    :promo_bulk_purchase
  ]
)
```

If you want to define options for promotion, here you go: (*):

```ruby
Cabify::PricingRules.new(voucher: { promo_bulk_purchase: { x: 2, reduced_price: Money.new(450, 'EUR') }}) 
```

(*) Currently only `bulk-purchase` does support this feature. Please notice that `reduced_price` is an Money object.

Isn't it awesome? Finally, you can something like:

```ruby
Cabify::PricingRules.new(
  voucher: [
    { promo_bulk_purchase: { x: 4, reduced_price: Money.new(100, Cabify::Checkout::CURRENCY) } },
    :promo_2_for_1,
  ]
)
```

It combines 2-for-1 and bulk purchase discounts and you don't need to hardcode anything in the code!
The only one thing you have to do is just pass options dynamically!

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Running tests

```
$ rspec spec
```

## Suggestions / Todo's

1. ~~Use money gem (We should avoid representing monetary values with floating point types so to avoid a whole class of errors relating to lack of precision);~~
2. ~~Validation for pricing rules;~~
3. ~~Introduce line item object;~~
4. ~~Store more details in cart (i.e. item details);~~
5. ~~Allow to apply a few promotions for the same item;~~
6. Add ability to configure CURRENCY.
7. ~~Allow to define pricing_rules explicity by passing options in a Hash.~~

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/rafaltrojanowski/cabify-checkout. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Cabify::Checkout project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/cabify-checkout/blob/master/CODE_OF_CONDUCT.md).
