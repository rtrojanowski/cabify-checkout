module Cabify
  class LineItem

    attr_accessor :item, :quantity

    def initialize(item)
      @item     = item
      @quantity = 1
    end

    def discount(pricing_rules)
      Cabify::DiscountCaculator.new(item, quantity, pricing_rules).discount
    end

  end
end
