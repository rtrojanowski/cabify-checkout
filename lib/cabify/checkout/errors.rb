module Cabify
  class Checkout
    class ItemNotSupported < StandardError

      attr_reader :item

      def initialize(message = nil, item = nil)
        @item = item
        message << " Please use upcased version i.e. #{item.upcase}." if upcase_info?
        message << " Currently supported items: #{ALLOWED_ITEMS.join(', ')}." if supported_items_info?
        super(message)
      end

      private

        def upcase_info?
          ALLOWED_ITEMS.include?(item.upcase)
        end

        def supported_items_info?
          !upcase_info?
        end
    end
  end
end
