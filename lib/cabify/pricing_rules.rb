require_relative 'discount_calculator'

module Cabify
  class PricingRules

    class NotValidError < StandardError; end

    AVAILABLE_PROMOTIONS = %i(promo_bulk_purchase promo_2_for_1 promo_3_for_1)

    attr_accessor(
      :mug,
      :tshirt,
      :voucher
    )

    def initialize(mug: nil, tshirt: nil, voucher: nil)
      @mug     = mug
      @tshirt  = tshirt
      @voucher = voucher

      validate_attributes!
    end


    def validate_attributes!
      [mug, tshirt, voucher].each do |attr|
        next if attr.nil? || attr.is_a?(Array) || attr.is_a?(Hash)
        raise NotValidError unless AVAILABLE_PROMOTIONS.include?(attr)
      end
    end

  end
end
