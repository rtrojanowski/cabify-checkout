module Cabify
  class Cart

    # NOTE: Cart is the eqivalent of an Order.
    # Cart has many line_items.

    attr_accessor :pricing_rules
    attr_reader :line_items

    def initialize(pricing_rules = PricingRules.new)
      @pricing_rules = pricing_rules
      @line_items    = []
    end

    def add_item(item_code)
      item = item_klass(item_code).new
      line_item = find_line_item_by(code: item.code)
      line_item ? line_item.quantity += 1 : add_line_item(item)
      item
    end

    def add_line_item(item)
      @line_items << LineItem.new(item)
    end

    def find_line_item_by(code: nil)
      @line_items.select { |lm| lm.item.code == code }.first
    end

    def total
      total = 0

      line_items.each do |line_item|
        item = line_item.item
        quantity = line_item.quantity
        subtotal = item.price * quantity
        subtotal = subtotal - line_item.discount(pricing_rules)
        total += subtotal
      end

      total
    end

    private

      def item_klass(item_code)
        klass_name = item_module_prefix.concat(item_code.downcase.capitalize)
        klass = Object.const_get(klass_name)
      end

      def item_module_prefix
        "Cabify::Product::"
      end
  end
end
