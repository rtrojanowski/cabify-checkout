require_relative 'promo_base'

module Cabify
  module PricingRule
    class PromoNFor1 < PromoBase

      def initialize(*args)
        super
      end

      def discount
        return Cabify::Checkout::ZERO_DISCOUNT if free_items_count.zero?
        discount = free_items_count * item.price
      end

      def free_items_count
        free_items_count = quantity / n
      end

      def n
        fail NotImplementedError, "A child class must be able to #n"
      end
    end
  end
end
