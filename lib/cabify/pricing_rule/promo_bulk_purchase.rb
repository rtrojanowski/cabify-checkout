require_relative 'promo_base'

module Cabify
  module PricingRule
    class PromoBulkPurchase < PromoBase

      # Buying x or more of a product, the price of that product is reduced.
      # For x value and reduced price value please take look into item class

      attr_reader :x, :reduced_price

      def initialize(item, quantity, options = {})
        @x = options.fetch(:x, item.bulk_purchase_min_qty)
        @reduced_price = options.fetch(:reduced_price, item.reduced_price)
        super(item, quantity)
      end

      def discount
        return Cabify::Checkout::ZERO_DISCOUNT if quantity < x
        discount = (item.price - reduced_price) * quantity
      end
    end

  end
end
