require_relative 'promo_n_for_1'

module Cabify
  module PricingRule
    class Promo2For1 < PromoNFor1

      def n
        2
      end

    end
  end
end
