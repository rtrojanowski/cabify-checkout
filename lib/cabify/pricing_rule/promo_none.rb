require_relative 'promo_base'

module Cabify
  module PricingRule
    class PromoNone < PromoBase

      def initialize(*args)
        super(*args)
      end

      def discount
        Cabify::Checkout::ZERO_DISCOUNT
      end

    end
  end
end
