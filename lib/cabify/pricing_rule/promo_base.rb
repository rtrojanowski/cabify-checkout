module Cabify
  module PricingRule
    class PromoBase

      def initialize(item, quantity, options = {})
        @item     = item
        @quantity = quantity
        @options  = options
      end

      def discount
        fail NotImplementedError, "A child class must be able to #discount"
      end

      private

        attr_reader :item, :quantity, :options

    end
  end
end
