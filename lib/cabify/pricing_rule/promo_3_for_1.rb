require_relative 'promo_n_for_1'

module Cabify
  module PricingRule
    class Promo3For1 < PromoNFor1

      def n
        3
      end

    end
  end
end
