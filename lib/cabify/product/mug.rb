module Cabify
  module Product
    class Mug < Item

      def code
        'MUG'.freeze
      end

      def name
        'Cabify Coffee Mug'.freeze # there must be a human-typing error the doc i.e. Cafify
      end

      def price
        Money.new(750, Cabify::Checkout::CURRENCY)
      end

      def to_sym
        :mug
      end

    end
  end
end
