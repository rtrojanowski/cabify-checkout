module Cabify
  module Product
    module TshirtBulkPurchase

      def reduced_price
        Money.new(1900, Cabify::Checkout::CURRENCY)
      end

      def bulk_purchase_min_qty
        3
      end

    end
  end
end

