module Cabify
  module Product
    module VoucherBulkPurchase

      def reduced_price
        Money.new(400, Cabify::Checkout::CURRENCY)
      end

      def bulk_purchase_min_qty
        3
      end

    end
  end
end

