require_relative 'item'
require_relative 'tshirt_bulk_purchase'

module Cabify
  module Product
    class Tshirt < Item

      include TshirtBulkPurchase

      def code
        'TSHIRT'.freeze
      end

      def name
        'Cabify T-Shirt'.freeze
      end

      def price
        Money.new(2000, Cabify::Checkout::CURRENCY)
      end

      def to_sym
        :tshirt
      end

    end
  end
end
