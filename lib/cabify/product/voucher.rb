require_relative 'item'
require_relative 'voucher_bulk_purchase'

module Cabify
  module Product
    class Voucher < Item

      include VoucherBulkPurchase

      def code
        'VOUCHER'.freeze
      end

      def name
        'Cabify Voucher'.freeze
      end

      def price
        Money.new(500, Cabify::Checkout::CURRENCY)
      end

      def to_sym
        :voucher
      end

    end
  end
end
