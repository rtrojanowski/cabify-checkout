module Cabify
  module Product
    class Item

      attr_reader :code, :name, :price

      def initialize
        @code  = code
        @name  = name
        @price = price
      end

      def to_h
        {
          code: code,
          name: name,
          price: price
        }
      end

      def to_s
        "#{code} | #{name} | #{sprintf "%.2f", price}€"
      end

      def reduced_price
        fail NotImplementedError, "#{self.class.name} does not implement #reduced_price yet."
      end

      def bulk_purchase_min_qty
        fail NotImplementedError, "#{self.class.name} does not implement #bulk_purchase_min_qty yet."
      end

    end
  end
end
