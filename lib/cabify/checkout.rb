require "cabify/checkout/version"
require "cabify/checkout/errors"
require "cabify/product/item"
require "cabify/product/voucher"
require "cabify/product/tshirt"
require "cabify/product/mug"
require "cabify/cart"
require "cabify/pricing_rules"
require "cabify/line_item"

require 'money'

I18n.enforce_available_locales = false # TODO move it to initializer https://github.com/RubyMoney/money/issues/593

module Cabify
  class Checkout
    extend Forwardable

    ALLOWED_ITEMS = %w(VOUCHER TSHIRT MUG).freeze
    CURRENCY = "EUR".freeze
    ZERO_DISCOUNT = Money.new(0, CURRENCY)

    attr_accessor :cart

    def initialize(pricing_rules = PricingRules.new)
      @pricing_rules      = pricing_rules
      @cart               = Cart.new
      @cart.pricing_rules = pricing_rules
    end

    def scan(item_code)
      fail ItemNotSupported.new("#{item_code} is not supported.", item_code) unless ALLOWED_ITEMS.include?(item_code)
      @cart.add_item(item_code)
    end

    def total(format: true)
      total = Money.new(cart.total, CURRENCY)
      total = total.format if format
      total
    end

  end
end
