require_relative 'pricing_rule/promo_2_for_1'
require_relative 'pricing_rule/promo_3_for_1'
require_relative 'pricing_rule/promo_bulk_purchase'
require_relative 'pricing_rule/promo_none'

module Cabify
  class DiscountCaculator

    attr_reader :item, :quantity, :pricing_rules

    def initialize(item, quantity, pricing_rules)
      @item = item
      @quantity = quantity
      @pricing_rules = pricing_rules
    end

    def discount
      return Cabify::Checkout::ZERO_DISCOUNT if pricing_rules.nil?

      discounts = Array.new(1, Cabify::Checkout::ZERO_DISCOUNT)
      discounts.concat(pricing_rule_services.map(&:discount))
      discounts.inject(:+)
    end

    private

      # @return [Array] (of initialized Cabify::PricingRule child object(s)
      #
      # @example
      # [#<Cabify::PricingRule::PromoBulkPurchase:0x007fb21d21d118
      # @x=4,
      # @reduced_price=#<Money fractional:100 currency:EUR>,
      # @item=#<Cabify::Product::Voucher:0x007fb21d21d8e8,
      # @quantity=4>]
      #
      def pricing_rule_services
        @services ||= pricing_rule_klasses.map do |array|
          if array.is_a? Array
            klass_name = array.first
            attrs = array.last

            klass = Object.const_get(promo_module_prefix.concat(klass_name))
            klass.new(item, quantity, attrs)
          else
            raise "Wrong structure has been passed: #{pricing_rules.inspect}"
          end
        end
      end

      # @return [Array] (of Strings or 2-element Arrays)
      #
      # @example
      # ["Promo2For1", ["PromoBulkPurchase", {:x=>1, :reduced_price=>1}]]
      #
      # @see #pricing_rule_services
      def pricing_rule_klasses
        @pricing_rule_klasses = []

        item_sym = item.to_sym
        pricing_rule = pricing_rules.send(item_sym)

        rules = begin
                  if pricing_rule.is_a?(Hash) || pricing_rule.is_a?(Symbol)
                    [pricing_rule]
                  elsif pricing_rule.is_a? Array
                    pricing_rule
                  else
                    [:promo_none]
                  end
                end

        rules.each { |obj| @pricing_rule_klasses << transform(obj) }

        @pricing_rule_klasses
      end

      # @returns Array
      #   - first element is class name
      #   - second element contains hash of attributes
      def transform(object)
        if object.is_a? Symbol
          klass_name = klassify.call(object)
          attrs = {}
        else object.is_a? Hash
          klass_name = klassify.call(object.keys.first)
          attrs = object[object.keys.first]
        end

        [klass_name, attrs]
      end

      # Transfor symbol to camel-cased string i.e. :sample_symbol -> "SampleSymbol"
      def klassify
        lambda { |sym| sym.to_s.split("_").collect(&:capitalize).join }
      end

      def promo_module_prefix
        "Cabify::PricingRule::"
      end
  end
end
