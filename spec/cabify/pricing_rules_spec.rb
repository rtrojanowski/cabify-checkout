RSpec.describe Cabify::PricingRules do


  describe 'valiations' do
    context 'when pricing rule is invalid' do
      subject { described_class.new(mug: :batch_purchase) } # promo prefix missing
      it 'raises an error' do
        expect { subject }.to raise_error(Cabify::PricingRules::NotValidError)
      end
    end

    context 'when pricing rule is valid' do
      subject { described_class.new(mug: :promo_bulk_purchase) }
      it 'does not raise an error' do
        expect { subject }.to_not raise_error(Cabify::PricingRules::NotValidError)
      end
    end
  end
end
