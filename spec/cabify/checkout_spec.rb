RSpec.describe Cabify::Checkout do

  it "has a version number" do
    expect(Cabify::Checkout::VERSION).not_to be nil
  end

  describe '#initialize' do
    let(:not_valid_pricing_rules) {
      Cabify::PricingRules.new(mug: :batch_purchase)
    }

    subject { described_class.new(not_valid_pricing_rules) }

    context 'when pricing rules are not valid' do
      it 'raise an error' do
        expect { subject }.to raise_error(Cabify::PricingRules::NotValidError)
      end
    end
  end

  describe '#scan' do
    subject { described_class.new }

    context 'when item is not supported' do
      it "returns an error with currently supported items information" do
        checkout = Cabify::Checkout.new
        expect { subject.scan("MASCOT") }.to raise_error(
          Cabify::Checkout::ItemNotSupported, 'MASCOT is not supported. Currently supported items: VOUCHER, TSHIRT, MUG.'
        )
      end

      it "returns an error with hint" do
        checkout = Cabify::Checkout.new
        expect { subject.scan("voucher") }.to raise_error(
          Cabify::Checkout::ItemNotSupported, 'voucher is not supported. Please use upcased version i.e. VOUCHER.'
        )
      end
    end
  end

  describe '#total' do
    context 'when cart is empty' do
      it 'returns 0.00€' do
        expect(subject.total).to eq("€0,00")
      end
    end

    context 'when cart total has been calculated' do
      let(:cart) { double(total: 9900) }

      before { subject.cart = cart }

      it 'returns formatted cart`s total value by default' do
        expect(subject.total).to eq("€99,00")
      end

      it 'returns cart`s total value as Money object when format == false' do
        expect(subject.total(format: false)).to eq(Money.new(9900, 'EUR'))
      end
    end
  end
end
