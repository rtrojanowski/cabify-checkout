RSpec.describe Cabify::LineItem do

  let(:item) { Cabify::Product::Voucher.new }
  subject { described_class.new(item) }

  describe '#item' do
    it 'returns item' do
      expect(subject.item).to eq(item)
    end
  end

  describe '#quantity' do
    it 'returns quantity' do
      expect(subject.quantity).to eq(1)
    end
  end

  describe '#discount' do
    context 'when two promos has been applied' do
      let(:pricing_rules) { Cabify::PricingRules.new(voucher: [:promo_2_for_1, :promo_bulk_purchase]) }

      it 'returns total discount for combined promotions (14)' do
        # 1) 2-for-1) (4 / 2) * 5 = 10
        # 2) bulk purchase 4 * 5 - 4 * 4 = 4
        # 1) + 2) = 14

        subject.quantity = 4
        expect(subject.discount(pricing_rules)).to be_an Money
        expect(subject.discount(pricing_rules).format).to eq("€14,00")
      end
    end

    context 'when one promo has been applied' do
      let(:pricing_rules) { Cabify::PricingRules.new(voucher: :promo_2_for_1) }

      it 'returns discount (4 / 2) * 5 = 10' do
        subject.quantity = 4
        expect(subject.discount(pricing_rules)).to be_an Money
        expect(subject.discount(pricing_rules).format).to eq("€10,00")
      end
    end

    context 'when nil has been passed in' do
      let(:pricing_rules) { Cabify::PricingRules.new(voucher: nil) }

      it 'returns 0 as Money object' do
        subject.quantity = 4
        expect(subject.discount(pricing_rules)).to be_an Money
        expect(subject.discount(pricing_rules).format).to eq("€0,00")
      end
    end

    context 'when Hash has been passed in' do
      let(:reduced_price) { Money.new(100, Cabify::Checkout::CURRENCY) }
      let(:quantity) { 4 }

      before { subject.quantity = quantity }

      context 'as a Hash' do
        let(:pricing_rules) { Cabify::PricingRules.new(
          voucher: {
            promo_bulk_purchase: {
              x: quantity,
              reduced_price: reduced_price
            }
          })
        }

        it 'returns 16,00(€) as Money object' do
          expected_discount = (subject.item.price * subject.quantity) - (reduced_price * quantity)

          expect(subject.discount(pricing_rules)).to be_an Money
          expect(subject.discount(pricing_rules).format).to eq(expected_discount.format)
        end
      end

      context 'in an Array' do
        let(:pricing_rules) { Cabify::PricingRules.new(
          voucher: [
            promo_bulk_purchase: { x: quantity, reduced_price: reduced_price }
          ])
        }

        it 'returns 16,00(€) as Money object' do
          expected_discount = (subject.item.price * subject.quantity) - (reduced_price * quantity)

          expect(subject.discount(pricing_rules)).to be_an Money
          expect(subject.discount(pricing_rules).format).to eq(expected_discount.format)
        end
      end

      context 'within an Array with another element' do
        let(:reduced_price) { Money.new(100, Cabify::Checkout::CURRENCY) }
        let(:quantity) { 4 }

        let(:pricing_rules) { Cabify::PricingRules.new(
          voucher: [
            { promo_bulk_purchase: { x: quantity, reduced_price: reduced_price }},
            :promo_2_for_1
          ])
        }

        it 'returns 16,00(€) + 10,00(€) as Money object' do
          expect(subject.discount(pricing_rules)).to be_an Money
          expect(subject.discount(pricing_rules).format).to eq("€26,00")
        end
      end
    end
  end
end
