module Helpers

  def process_checkout(cart, items)
    items.each { |item| cart.add_item(item) }
    cart
  end

end
