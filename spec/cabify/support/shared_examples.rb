RSpec.shared_examples_for "a item" do

  subject { described_class.new }

  describe 'to_h' do
    it "returns hash representation of object" do
      expect(subject.to_h).to eq(
        {
          code: subject.code,
          name: subject.name,
          price: subject.price
        }
      )
    end
  end
end
