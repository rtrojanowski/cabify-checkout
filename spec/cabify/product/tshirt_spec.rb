require_relative '../support/shared_examples'

RSpec.describe Cabify::Product::Tshirt do

  subject { described_class.new }

  describe 'code' do
    it "returns TSHIRT" do
      expect(subject.code).to eq 'TSHIRT'
    end
  end

  describe 'name' do
    it "returns Cabify T-Shirt" do
      expect(subject.name).to eq 'Cabify T-Shirt'
    end
  end

  describe 'price' do
    it "returns 20.00 (€)" do
      expect(subject.price).to be_an Money
      expect(subject.price.format).to eq "€20,00"
    end
  end

  include_examples "a item"

end
