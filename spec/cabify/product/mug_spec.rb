require_relative '../support/shared_examples'

RSpec.describe Cabify::Product::Mug do

  subject { described_class.new }

  describe 'code' do
    it "returns MUG" do
      expect(subject.code).to eq 'MUG'
    end
  end

  describe 'name' do
    it "returns Cabify Coffee Mug" do
      expect(subject.name).to eq 'Cabify Coffee Mug'
    end
  end

  describe 'price' do
    it "returns 7.50 (€) as a Money object" do
      expect(subject.price).to be_an Money
      expect(subject.price.format).to eq "€7,50"
    end
  end

  include_examples "a item"

end
