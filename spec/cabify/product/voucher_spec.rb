require_relative '../support/shared_examples'

RSpec.describe Cabify::Product::Voucher do

  subject { described_class.new }

  describe 'code' do
    it "returns VOUCHER" do
      expect(subject.code).to eq 'VOUCHER'
    end
  end

  describe 'name' do
    it "returns Cabify Voucher" do
      expect(subject.name).to eq 'Cabify Voucher'
    end
  end

  describe 'price' do
    it "returns 5.00 (€)" do
      expect(subject.price).to be_an Money
      expect(subject.price.format).to eq "€5,00"
    end
  end

  include_examples "a item"

end
