RSpec.describe Cabify::Cart do

  describe '#add_item' do
    let(:cart) { described_class.new }

    it 'stores line items with quantity' do
      cart.add_item("VOUCHER")
      expect(cart.line_items.size).to eq 1
      cart.add_item("VOUCHER")
      expect(cart.line_items.size).to eq 1
      cart.add_item("TSHIRT")
      expect(cart.line_items.size).to eq 2
      expect(cart.line_items.first.quantity).to eq 2
      expect(cart.line_items.last.quantity).to eq 1
    end
  end

  describe "#total" do
    context 'when pricing rules has been applied' do
      let(:pricing_rules) do
        Cabify::PricingRules.new(
          {
            voucher: :promo_2_for_1,
            tshirt: :promo_bulk_purchase,
            mug: nil
          }
        )
      end

      let(:cart) { described_class.new(pricing_rules) }
      let(:full_cart) {  process_checkout(cart, items) }

      subject { full_cart }

      context 'and when any promotion has been granted' do
        let(:items) { %w(VOUCHER TSHIRT MUG) }

        it 'returns total amount to be paid' do
          expect(subject.total).to be_an Money
          expect(subject.total.format).to eq('€32,50')
        end
      end

      context 'and when 2-for-1 for VOUCHER has been granted' do
        let(:items) { %w(VOUCHER TSHIRT VOUCHER) }

        it 'returns total including discount for VOUCHER' do
          expect(subject.total).to be_an Money
          expect(subject.total.format).to eq('€25,00')
        end
      end

      context 'and when bulk purchases on TSHIRT has been granted' do
        let(:items) { %w(TSHIRT TSHIRT TSHIRT VOUCHER TSHIRT) }

        it 'returns total including discount for TSHIRT' do
          expect(subject.total).to be_an Money
          expect(subject.total.format).to eq('€81,00')
        end
      end

      context 'and when 2-for-1 (VOUCHER) and bulk purchases (TSHIRT) granted' do
        let(:items) { %w(VOUCHER TSHIRT VOUCHER VOUCHER MUG TSHIRT TSHIRT) }

        it 'returns total including discount for VOUCHER and TSHIRT' do
          expect(subject.total).to be_an Money
          expect(subject.total.format).to eq('€74,50')
        end
      end
    end

    context 'when any promotion has been applied' do
      let(:cart) { described_class.new }
      let(:full_cart) {  process_checkout(cart, items) }
      let(:items) { %w(VOUCHER TSHIRT VOUCHER VOUCHER MUG TSHIRT TSHIRT) }

      subject { full_cart }

      it 'returns total amount to be paid without discounts' do
        expect(subject.total).to be_an Money
        expect(subject.total.format).to eq('€82,50')
      end
    end
  end
end
